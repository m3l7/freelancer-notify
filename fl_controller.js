var flns_config = {
	"cdn_urls":["cdn2.f-cdn.com","cdn3.f-cdn.com","cdn5.f-cdn.com","cdn6.f-cdn.com"],
	"cookie_base":"GETAFREE",
	"notify_server":"//notifications.freelancer.com",
	"site_name":"Freelancer.com",
	"cookie_domain":".freelancer.com"
}

var fl_status = 'uninitialized' 
var flSock
var user_jobs = []

var change_status = function(status){
	console.log('status: '+status)
	// console.log(new Error(status).stack)
	fl_status = status
}

var getLoginInfo = function(){
	change_status('gettingLoginInfo')

	$.post('https://www.freelancer.com/ajax/pinky/header.php',function(data){
		data = JSON.parse(data)
		if ((!data.userState) || (data.userState!=1)) change_status('disconnected')
		else change_status('logged')
		if ((!!data.data) && (data.data.userJobs)) user_jobs = data.data.userJobs
	})
}

var startSockets = function(){
	console.log('----- Starting Sockets!')
    var h = {hash: '',hash2: '',user_id: '',channels: user_jobs};

    change_status('startingSockets')

	chrome.cookies.getAll({},function(cookies){
		for (var i = cookies.length - 1; i >= 0; i--) {
			var cookie = cookies[i]
			if (cookie.name=='GETAFREE_AUTH_HASH') h.hash = cookie.value
			if (cookie.name=='GETAFREE_AUTH_HASH_V2') h.hash2 = cookie.value
			if (cookie.name=='GETAFREE_USER_ID') h.user_id = cookie.value
		};

		if ((!h.hash) || (!h.hash2) || (!h.user_id)) change_status('disconnected')
		else change_status('startingSockets')

		flSock = new SockJS("//notifications.freelancer.com");	
    	
		flSock.onmessage = function(d) {
		    var e = JSON.parse(d.data);

		    if ((e.channel=='subscribe') && (e.body=='NO')){
		    	change_status('disconnected')
		    }
		    if ((e.channel=='subscribe') && (e.body=='OK')){
		    	change_status('running')
		    }
		    else if (e.channel=='user'){
		    	change_status('running')
		    	var data = e.body.data
		    	var url = 'https://www.freelancer.com'+data.linkUrl
		    	var price = '('+data.minbudget+' - '+data.maxbudget+' '+data.currency+')'

		    	var notification = new Notification(data.title+' '+price,{
		    		icon: data.imgUrl,
		    		body: data.appended_descr
		    	})
		    	notification.onclick = function(){
		    		notification.close()
		    		window.open(url)
		    	}
		    	window.setTimeout(function() {notification.close()}, 10000);
		    } 
		}
		flSock.onopen = function() {
		    flSock.send(JSON.stringify({channel: 'auth',body: h}))
		}
		flSock.onclose = function(a) {
			// if (a.code==1000){
		    	change_status('stop')
			// }
		}
	})

}




//RUNTIME CODE


//initialize automatically the script at startup
fl_status = 'initializing'
getLoginInfo()
setInterval(function(){
	if (fl_status!='gettingLoginInfo'){
		clearInterval()
		if (fl_status=='logged') startSockets()
	}
},200)


chrome.runtime.onMessage.addListener(function(message,sender,responseCallback){

	if ((!!message) && (!!message.action)){

		// GET STATUS
			if (message.action=='status'){
				if (!!responseCallback) responseCallback(fl_status)
			}

		// LOG IN
			if (message.action=='login'){
				var user = message.user
				var pass = message.pass
				change_status('logging')
				if (!!responseCallback) responseCallback(fl_status)

				$.post("https://www.freelancer.com/users/ajaxonlogin.php?username="+user+"&passwd="+pass+"&savelogin=on",function(data){
					data = JSON.parse(data)
					if ((!data.status) || (data.status=='error')) change_status('disconnected')
					else change_status('logged')
				})
			}

		// START
			if (message.action=='start'){
				if ((fl_status!='stop') && (fl_status!='uninitialized')){
					if (!!responseCallback) responseCallback(fl_status)
				}
				else getLoginInfo()
				if (!!responseCallback) responseCallback(fl_status)
			}

		// STOP WEBSOCKET
			if (message.action=='stop'){
				flSock.close()
				change_status('stop')
				if (!!responseCallback) responseCallback('stop')
			}

		// START WEBSOCKETS
			if (message.action=='listenNotifications'){
				console.log('asdasd')
				startSockets(function(){
					if (!!responseCallback) {
						console.log('asdasd '+fl_status)
						responseCallback(fl_status)
					}
				})
			}

		return true
	}
})

